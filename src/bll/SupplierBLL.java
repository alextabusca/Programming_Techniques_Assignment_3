package bll;

import java.util.List;
import java.util.NoSuchElementException;

import dao.SupplierDAO;
import model.Supplier;

public class SupplierBLL {

    private SupplierDAO supplierDAO;

    public SupplierBLL() {
        supplierDAO = new SupplierDAO();
    }

    public Supplier findSupplierById(int id) {
        Supplier sp = supplierDAO.findById(id);
        if (sp == null) {
            throw new NoSuchElementException("The supplier with id =" + id + " was not found!");
        }
        return sp;
    }

    public boolean insertSupplier(Supplier supplier){
        if (supplierDAO.insert(supplier) == false) {
            System.out.println("Error while inserting a supplier");
            return false;
        } else {
            System.out.println("Supplier inserted successfully");
            return true;
        }
    }

    public boolean deleteSupplier(int id) {
        if (supplierDAO.deleteById(id) == false) {
            System.out.println("Error while deleting supplier");
            return false;
        } else {
            System.out.println("Supplier deleted successfully");
            return true;
        }
    }

    public boolean updateSupplier(Supplier sp) {
        if (supplierDAO.updateById(sp) == false) {
            System.out.println("Error while updating a supplier");
            return false;
        } else {
            System.out.println("Supplier updated successfully");
            return true;
        }
    }

    public List<Supplier> findAll(){
        return supplierDAO.findAll();
    }

}
