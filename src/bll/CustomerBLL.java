package bll;

import java.util.List;
import java.util.NoSuchElementException;

import dao.CustomerDAO;
import model.Customer;

public class CustomerBLL {

    private CustomerDAO customerDAO;

    public CustomerBLL(){
        customerDAO = new CustomerDAO();
    }

    public Customer findCustomerById(int id) {
        Customer sp = customerDAO.findById(id);
        if (sp == null) {
            throw new NoSuchElementException("The Customer with id =" + id + " was not found!");
        }
        return sp;
    }

    public boolean insertCustomer(Customer Customer){
        if (customerDAO.insert(Customer) == false) {
            System.out.println("Error while inserting a Customer");
            return false;
        } else {
            System.out.println("Customer inserted successfully");
            return true;
        }
    }

    public boolean deleteCustomer(int id) {
        if (customerDAO.deleteById(id) == false) {
            System.out.println("Error while deleting Customer");
            return false;
        } else {
            System.out.println("Customer deleted successfully");
            return true;
        }
    }

    public boolean updateCustomer(Customer sp) {
        if (customerDAO.updateById(sp) == false) {
            System.out.println("Error while updating a Customer");
            return false;
        } else {
            System.out.println("Customer updated successfully");
            return true;
        }
    }

    public List<Customer> findAll(){
        return customerDAO.findAll();
    }


}
