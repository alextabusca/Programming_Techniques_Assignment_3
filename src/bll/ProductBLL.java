package bll;

import java.util.List;
import java.util.NoSuchElementException;

import dao.ProductDAO;
import model.Product;

public class ProductBLL {

    private ProductDAO productDAO;

    public ProductBLL(){
        productDAO = new ProductDAO();
    }

    public Product findProductById(int id) {
        Product sp = productDAO.findById(id);
        if (sp == null) {
            throw new NoSuchElementException("The Product with id =" + id + " was not found!");
        }
        return sp;
    }

    public boolean insertProduct(Product Product){
        if (productDAO.insert(Product) == false) {
            System.out.println("Error while inserting a Product");
            return false;
        } else {
            System.out.println("Product inserted successfully");
            return true;
        }
    }

    public boolean deleteProduct(int id) {
        if (productDAO.deleteById(id) == false) {
            System.out.println("Error while deleting Product");
            return false;
        } else {
            System.out.println("Product deleted successfully");
            return true;
        }
    }

    public boolean updateProduct(Product sp) {
        if (productDAO.updateById(sp) == false) {
            System.out.println("Error while updating a Product");
            return false;
        } else {
            System.out.println("Product updated successfully");
            return true;
        }
    }

    public List<Product> findAll(){
        return productDAO.findAll();
    }


}
