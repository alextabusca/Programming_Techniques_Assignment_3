package bll;

import dao.OrderDAO;
import model.Order;

public class OrderBLL {

    private OrderDAO orderDAO;

    public OrderBLL(){
        orderDAO = new OrderDAO();
    }

    public boolean insertOrder(Order order){
        if (orderDAO.insert(order) == false) {
            System.out.println("Error while inserting an Order");
            return false;
        } else {
            System.out.println("Order inserted successfully");
            return true;
        }
    }
}
