package start;

import java.sql.SQLException;

import gui.GUI;

public class App {

    public static void main(String[] args) throws SQLException{

        GUI uIntfCl = new GUI("Client");
        GUI uIntfPr = new GUI("Product");
        GUI uIntfBuy = new GUI("Buy");
        uIntfCl.setVisible(true);
        uIntfPr.setVisible(true);
        uIntfBuy.setVisible(true);

    }
}
