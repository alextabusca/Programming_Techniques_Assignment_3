package model;

public class Customer {

    private int id;
    private String name;

    public Customer(){
        this(0, "default");
    }

    public Customer(String name) {
        this(0, name);
    }

    public Customer(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public void setId(int id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public int getId() {
        return id;
    }

    @Override
    public String toString() {
        return "Customer[ " + id + "] has the name " + name;
    }

}
