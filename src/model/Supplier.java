package model;

public class Supplier {

    private int id;
    private String supplierName;

    public Supplier(){
    }

    public Supplier(String supplierName){
        this.supplierName = supplierName;
    }

    public Supplier(int id, String supplierName){
        this.id = id;
        this.supplierName = supplierName;
    }

    public String getSupplierName() {
        return supplierName;
    }

    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    @Override
    public String toString() {
        return "Supplier " + id + " is called " + supplierName;
    }
}
