package model;

public class Product {

    private int id;
    private int supplierId;
    private String name;
    private double price;
    private int quantity;



    public Product(int id, int supplierId, String name, double price, int quantity) {
        this.id = id;
        this.supplierId = supplierId;
        this.name = name;
        this.price = price;
        this.quantity = quantity;
    }

    public Product(int supplierId, String name, double price, int quantity){
        this(0, supplierId, name, price, quantity);
    }

    public Product(){
        this(0, 0, "def", 0, 0);
    }

    public int getSupplierId() {
        return supplierId;
    }

    public void setSupplierId(int supplierId) {
        this.supplierId = supplierId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }




    @Override
    public String toString() {
        return "Product [" + id + "] " + name + " has the supplier " + supplierId + ". Price: " + price + ", Quantity: " + quantity;
    }
}
