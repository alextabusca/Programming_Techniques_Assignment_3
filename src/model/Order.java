package model;

public class Order {

    private int id;
    private int customerId;
    private int productId;
    private int quantity;
    private double fPrice;

    public Order(int customerId, int productId, int quantity, double fPrice) {
        this.id = 0;
        this.customerId = customerId;
        this.productId = productId;
        this.quantity = quantity;
        this.fPrice = fPrice;
    }


    public int getCustomerId() {
        return customerId;
    }
    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }
    public int getProductId() {
        return productId;
    }
    public void setProductId(int productId) {
        this.productId = productId;
    }
    public int getQuantity() {
        return quantity;
    }
    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public double getFPrice() {
        return fPrice;
    }
    public void setFPrice(double fPrice) {
        this.fPrice = fPrice;
    }

    @Override
    public String toString() {
        return "Order [" + id + "] customer: " + customerId + " productId: " + productId + " quantity: " + quantity + " finalPrice: " + fPrice;
    }

}
