package exception;

public class NoResultException extends Exception{
    /**
     *
     */
    private static final long serialVersionUID = -3938261979463125859L;

    public NoResultException() {
        super("The query returned no results!");
    }
}
