package gui;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

public class ReflMethods {

    public static <T> MyTableModel createTable(List<T> objects) {

        MyTableModel model = new MyTableModel();

        //set column names
        for(Field field: objects.get(0).getClass().getDeclaredFields()){
            model.addColumn(field.getName());
        }

        for(T t: objects) {
            List <Object> data = new ArrayList<>();
            for(Field field: t.getClass().getDeclaredFields()){
                field.setAccessible(true);
                try {
                    data.add(field.get(t));
                } catch (IllegalArgumentException | IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
            model.addRow(data.toArray());
        }
        model.addRow(new Object[] {});

        return model;
    }

}
