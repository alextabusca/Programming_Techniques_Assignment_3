package gui;

import javax.swing.table.DefaultTableModel;

public class MyTableModel extends DefaultTableModel{

    /**
     *
     */
    private static final long serialVersionUID = 1404784549464548361L;

    @Override
    public boolean isCellEditable(int row, int column) {
        if (column == 0) return false;
        return true;
    }

    @Override
    public Class getColumnClass(int c) {
        return getValueAt(0, c).getClass();
    }

}
