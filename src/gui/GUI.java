package gui;

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.JTextField;

import bll.CustomerBLL;
import bll.OrderBLL;
import bll.ProductBLL;
import model.Customer;
import model.Order;
import model.Product;

public class GUI extends JFrame {


    private static final long serialVersionUID = 6219010821916810748L;
    private JTable products, clients;
    private ProductBLL productBll;
    private CustomerBLL customerBll;
    private OrderBLL orderBll;
    private MyTableModel tModel;
    private JTextField quantity;
    private JLabel infoCr; //current info
    private BufferedWriter output;
    private Double totalPrice;

    private final String FILE_NAME = "receipt.txt";
    private FileWriter fw;


    public GUI(String name) {

        productBll = new ProductBLL();
        customerBll = new CustomerBLL();
        orderBll = new OrderBLL();

        if (name.equals("Client")) {
            initUIClient();
        }
        if (name.equals("Product")) {
            initUIProduct();
        }
        if (name.equals("Buy")) {
            initUIBuy();
        }
    }

    private void initUIBuy() {

        totalPrice = 0.0;

        tModel = ReflMethods.createTable(productBll.findAll());
        products = new JTable(tModel);
        products.setSelectionMode(0);
        tModel = ReflMethods.createTable(customerBll.findAll());
        clients = new JTable(tModel);
        clients.setSelectionMode(0);

        quantity = new JTextField("0");
        JLabel quantityT = new JLabel("Quanity:");
        JButton add = new JButton("Add");
        JButton finish = new JButton("Finish Order");
        JLabel info = new JLabel("Please insert a positive Quantity and press the Add button. When the order is finished, click Finish Order");
        infoCr = new JLabel(" ");

        try {
            fw = new FileWriter(FILE_NAME);
        } catch (IOException e) {
            e.printStackTrace();
        }
        output = new BufferedWriter (fw);

        class AddListener implements ActionListener{

            @Override
            public void actionPerformed(ActionEvent e) {

                int crRow = products.getSelectedRow();
                if (crRow == -1 || crRow == -1) {
                    infoCr.setText("Please select a row in both tables!");
                    return;
                }

                int quant = Integer.parseInt(quantity.getText());
                int stock = (int)products.getValueAt(crRow, 4);
                if (quant <= 0) {
                    infoCr.setText("Please introduce a quantity greater than 0");
                    return;
                }
                if (quant > stock) {
                    infoCr.setText("Sorry, we don't have this quantity in stock");
                    return;
                }

                String customerName = (String)clients.getValueAt(clients.getSelectedRow(), 1);
                String productName = (String)products.getValueAt(crRow, 2);
                double price = (double)products.getValueAt(crRow, 3);
                price *= quant;

                Product prod = new Product((int)products.getValueAt(crRow, 0),
                        (int)products.getValueAt(crRow, 1),
                        (String)products.getValueAt(crRow, 2),
                        (double)products.getValueAt(crRow, 3),
                        ((int)products.getValueAt(crRow, 4) - quant)
                );

                productBll.updateProduct(prod);

                String receiptRow = ("Customer " + customerName + " bought " + quant + "x " + productName + " for a total of " + price + " lei\n");
                try {
                    output.write(receiptRow);
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
                totalPrice += price;

                infoCr.setText(receiptRow);

                tModel = ReflMethods.createTable(productBll.findAll());
                products.setModel(tModel);

                //insert Order in the Order table

                Order order = new Order(
                        (int)clients.getValueAt(clients.getSelectedRow(), 0),
                        (int)products.getValueAt(crRow, 0),
                        quant,
                        price
                );

                orderBll.insertOrder(order);

            }

        }

        class FinishListener implements ActionListener {

            @Override
            public void actionPerformed(ActionEvent e) {

                try {
                    infoCr.setText("Order finalized for a total of " + totalPrice + " lei");
                    output.write("Order finalized for a total of " + totalPrice + " lei\n\n-------------------------------------------------------\n");
                    output.flush();
                    totalPrice = 0.0;
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }

        }

        add.addActionListener(new AddListener());
        finish.addActionListener(new FinishListener());

        createLayoutBuy(products.getTableHeader(), products, clients.getTableHeader(), clients, quantityT, quantity, add, info, infoCr, finish);

        setTitle("Buy");
        setSize(800, 640);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

    private void initUIProduct() {

        JButton modify = new JButton("Modify");
        JButton insert = new JButton("Insert");
        JButton delete = new JButton("Delete");

        tModel = ReflMethods.createTable(productBll.findAll());
        products = new JTable(tModel);
        products.setSelectionMode(0);

        class ModifyListener implements ActionListener{

            @Override
            public void actionPerformed(ActionEvent e) {
                Product prod = new Product();
                int i = 0;

                if (products.getSelectedRow() == -1) {
                    System.out.println("Please select a row");
                    return;
                }
                if ((double)products.getValueAt(products.getSelectedRow(), 3) < 0) {
                    System.out.println("Please insert a price which is bigger than 0!");
                    return;
                }
                if ((int)products.getValueAt(products.getSelectedRow(), 4) < 0) {
                    System.out.println("Please insert a positive quantity!");
                    return;
                }

                for(Field field: Product.class.getDeclaredFields()){
                    try {
                        PropertyDescriptor pd = new PropertyDescriptor(field.getName(), Product.class);
                        pd.getWriteMethod().invoke(prod, products.getValueAt(products.getSelectedRow(), i));
                        i ++;
                    } catch (IntrospectionException e1) {
                        e1.printStackTrace();
                    } catch (IllegalAccessException e1) {
                        e1.printStackTrace();
                    } catch (IllegalArgumentException e1) {
                        e1.printStackTrace();
                    } catch (InvocationTargetException e1) {
                        e1.printStackTrace();
                    }
                }

                productBll.updateProduct(prod);

            }

        }

        class InsertListener implements ActionListener{
            @Override
            public void actionPerformed(ActionEvent e){
                Product prod = new Product();
                int i = 0;

                if (products.getSelectedRow() == -1) {
                    System.out.println("Please select a row");
                    return;
                }
                if ((double)products.getValueAt(products.getSelectedRow(), 3) < 0) {
                    System.out.println("Please insert a price which is bigger than 0!");
                    return;
                }
                if ((int)products.getValueAt(products.getSelectedRow(), 4) < 0) {
                    System.out.println("Please insert a positive quantity!");
                    return;
                }

                for(Field field: Product.class.getDeclaredFields()){
                    try {
                        if (!field.getName().equals("id")) {
                            PropertyDescriptor pd = new PropertyDescriptor(field.getName(), Product.class);
                            pd.getWriteMethod().invoke(prod, products.getValueAt(products.getSelectedRow(), i));
                        }
                        i ++;
                    } catch (IntrospectionException e1) {
                        e1.printStackTrace();
                    } catch (IllegalAccessException e1) {
                        e1.printStackTrace();
                    } catch (IllegalArgumentException e1) {
                        e1.printStackTrace();
                    } catch (InvocationTargetException e1) {
                        e1.printStackTrace();
                    }
                }

                productBll.insertProduct(prod);
                tModel = ReflMethods.createTable(productBll.findAll());
                products.setModel(tModel);
            }
        }

        class DeleteListener implements ActionListener{

            @Override
            public void actionPerformed(ActionEvent e) {
                if (products.getSelectedRow() == -1) {
                    System.out.println("Please select a row");
                    return;
                }

                //fetch the id
                int id = ((Integer)(products.getValueAt(products.getSelectedRow(), 0))).intValue();
                productBll.deleteProduct(id);
                tModel.removeRow(products.getSelectedRow());
            }

        }

        delete.addActionListener(new DeleteListener());
        modify.addActionListener(new ModifyListener());
        insert.addActionListener(new InsertListener());


        createLayout(products.getTableHeader(), products, modify, insert, delete);

        setTitle("Product Manager");
        setSize(640, 400);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }



    private void initUIClient() {

        JButton modify = new JButton("Modify");
        JButton insert = new JButton("Insert");
        JButton delete = new JButton("Delete");

        tModel = ReflMethods.createTable(customerBll.findAll());

        clients = new JTable(tModel);
        clients.setSelectionMode(0);

        class ModifyListener implements ActionListener{

            @Override
            public void actionPerformed(ActionEvent e) {
                Customer cust = new Customer();
                int i = 0;

                if (clients.getSelectedRow() == -1) {
                    System.out.println("Please select a row");
                    return;
                }

                for(Field field: Customer.class.getDeclaredFields()){
                    try {
                        PropertyDescriptor pd = new PropertyDescriptor(field.getName(), Customer.class);
                        pd.getWriteMethod().invoke(cust, clients.getValueAt(clients.getSelectedRow(), i));
                        i ++;
                    } catch (IntrospectionException e1) {
                        e1.printStackTrace();
                    } catch (IllegalAccessException e1) {
                        e1.printStackTrace();
                    } catch (IllegalArgumentException e1) {
                        e1.printStackTrace();
                    } catch (InvocationTargetException e1) {
                        e1.printStackTrace();
                    }
                }

                customerBll.updateCustomer(cust);

            }

        }

        class InsertListener implements ActionListener{
            @Override
            public void actionPerformed(ActionEvent e){
                Customer cus = new Customer();
                int i = 0;

                if (clients.getSelectedRow() == -1) {
                    System.out.println("Please select a row");
                    return;
                }

                for(Field field: Customer.class.getDeclaredFields()){
                    try {
                        if (!field.getName().equals("id")) {
                            PropertyDescriptor pd = new PropertyDescriptor(field.getName(), Customer.class);
                            pd.getWriteMethod().invoke(cus, clients.getValueAt(clients.getSelectedRow(), i));
                        }
                        i ++;
                    } catch (IntrospectionException e1) {
                        e1.printStackTrace();
                    } catch (IllegalAccessException e1) {
                        e1.printStackTrace();
                    } catch (IllegalArgumentException e1) {
                        e1.printStackTrace();
                    } catch (InvocationTargetException e1) {
                        e1.printStackTrace();
                    }
                }

                customerBll.insertCustomer(cus);
                tModel = ReflMethods.createTable(customerBll.findAll());
                clients.setModel(tModel);
            }
        }

        class DeleteListener implements ActionListener{

            @Override
            public void actionPerformed(ActionEvent e) {
                if (clients.getSelectedRow() == -1) {
                    System.out.println("Please select a row");
                    return;
                }

                //fetch the id
                int id = ((Integer)(clients.getValueAt(clients.getSelectedRow(), 0))).intValue();
                customerBll.deleteCustomer(id);
                tModel.removeRow(clients.getSelectedRow());

            }

        }

        delete.addActionListener(new DeleteListener());
        modify.addActionListener(new ModifyListener());
        insert.addActionListener(new InsertListener());

        createLayout(clients.getTableHeader(), clients, modify, insert, delete);

        setTitle("Client Manager");
        setSize(640, 400);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

    private void createLayout(JComponent...arg){

        Container pane = getContentPane();
        GroupLayout gl = new GroupLayout(pane);
        pane.setLayout(gl);

        gl.setAutoCreateContainerGaps(true);
        gl.setAutoCreateGaps(true);

        gl.setHorizontalGroup(
                gl.createParallelGroup(GroupLayout.Alignment.CENTER).addComponent(arg[0]).addComponent(arg[1])
                        .addGroup(gl.createSequentialGroup().addComponent(arg[2]).addComponent(arg[3]).addComponent(arg[4]))
        );

        gl.setVerticalGroup(
                gl.createSequentialGroup().addComponent(arg[0]).addComponent(arg[1])
                        .addGroup(gl.createParallelGroup().addComponent(arg[2]).addComponent(arg[3]).addComponent(arg[4]))
        );

    }

    private void createLayoutBuy(JComponent...arg){

        Container pane = getContentPane();
        GroupLayout gl = new GroupLayout(pane);
        pane.setLayout(gl);

        gl.setAutoCreateContainerGaps(true);
        gl.setAutoCreateGaps(true);

        gl.setHorizontalGroup(
                gl.createParallelGroup(GroupLayout.Alignment.CENTER)
                        .addGroup(gl.createSequentialGroup()
                                .addGroup(gl.createParallelGroup().addComponent(arg[0]).addComponent(arg[1]))
                                .addGroup(gl.createParallelGroup().addComponent(arg[2]).addComponent(arg[3])))
                        .addGroup(gl.createSequentialGroup().addComponent(arg[4], 50, 50, 50).addComponent(arg[5], 40, 60, 80).addComponent(arg[6])) //quantity
                        .addComponent(arg[7])
                        .addComponent(arg[8])
                        .addComponent(arg[9])
        );

        gl.setVerticalGroup(
                gl.createSequentialGroup()
                        .addGroup(gl.createParallelGroup()
                                .addGroup(gl.createSequentialGroup().addComponent(arg[0]).addComponent(arg[1]))
                                .addGroup(gl.createSequentialGroup().addComponent(arg[2]).addComponent(arg[3])))
                        .addGroup(gl.createParallelGroup().addComponent(arg[4]).addComponent(arg[5], 25, 25, 25).addComponent(arg[6]))
                        .addComponent(arg[7])
                        .addComponent(arg[8])
                        .addComponent(arg[9])
        );

    }

}