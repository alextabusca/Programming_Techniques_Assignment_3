package dao;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.ParameterizedType;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;

import connection.ConnectionFactory;
import exception.NoResultException;

public class AbstractDAO<T> {

    private final Class<T> type;

    @SuppressWarnings("unchecked")
    public AbstractDAO() {
        this.type = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }

    private String createSelectQuery(String field) {
        StringBuilder sb = new StringBuilder();

        sb.append("SELECT * FROM ");
        sb.append(this.type.getSimpleName());
        sb.append(" WHERE ");
        sb.append(field);
        sb.append(" =?");

        return sb.toString();
    }

    public T findById(int id) {


        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet result = null;

        connection = ConnectionFactory.getConnection();
        try {
            statement = connection.prepareStatement(createSelectQuery("id"));
            statement.setInt(1, id);
            result = statement.executeQuery();

            return createObject(result).get(0);

        }
        catch (NoResultException e) {
            e.printStackTrace();
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        finally {
            ConnectionFactory.close(statement);
            ConnectionFactory.close(result);
            ConnectionFactory.close(connection);
        }

        return null;
    }

    private List<T> createObject(ResultSet result) throws NoResultException{

        List <T> objects = new ArrayList<>();

        try {
            if (result.next() == false) {
                throw new NoResultException();
            }
            result.previous();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        try {
            while(result.next()) {
                T crObj = type.newInstance();
                for(Field field: type.getDeclaredFields()) {
                    Object value = result.getObject(field.getName());
                    PropertyDescriptor propDesc = new PropertyDescriptor(field.getName(), type);
                    propDesc.getWriteMethod().invoke(crObj, value);
                }
                objects.add(crObj);
            }
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (IntrospectionException e) {
            e.printStackTrace();
        }

        return objects;

    }

    /**
     *
     * @param field the fields of the current object
     * @return a string representing a query for adding an object
     */
    private String createInsertQuery(List <String> field) {
        StringBuilder sb = new StringBuilder();
        StringJoiner sj = new StringJoiner(",", "(", ")");

        sb.append("INSERT INTO shop.");
        sb.append(type.getSimpleName());
        sb.append(" ");
        for (String s: field){
            sj.add(s);
        }

        sb.append(sj.toString());

        sb.append(" VALUES ");
        sj = new StringJoiner(",", "(", ")");

        for (int i = 0; i < field.size(); ++ i) {
            sj.add("?");
        }
        sb.append(sj.toString());

        return sb.toString();
    }

    /**
     *
     * @param t - Object to insert
     * @return true or false, telling if the insert was successful or not
     */
    public boolean insert (T t) {
        List <String> fields = new ArrayList<>();
        List <Object> fieldValues = new ArrayList<>();

        try {
            for(Field field: type.getDeclaredFields()) {
                if (field.getName() != "id") {
                    fields.add(field.getName());

                    PropertyDescriptor pd = new PropertyDescriptor(field.getName(), type);
                    fieldValues.add(pd.getReadMethod().invoke(t));
                }
            }
        }
        catch (IntrospectionException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }

        Connection connection = null;
        PreparedStatement statement = null;

        try {
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(createInsertQuery(fields));

            for (int i = 0; i < fields.size(); ++ i) {
                statement.setString(i + 1, fieldValues.get(i).toString());
            }

            statement.executeUpdate();

            return true;

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }

        return false;
    }

    private String createDeleteQuery(String field){
        StringBuilder sb = new StringBuilder();

        sb.append("DELETE FROM ");
        sb.append(type.getSimpleName());
        sb.append(" WHERE ");
        sb.append(field);
        sb.append("=?");

        return sb.toString();
    }

    public boolean deleteById(int id) {
        Connection connection = null;
        PreparedStatement statement = null;

        try {
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(createDeleteQuery("id"));
            statement.setInt(1, id);
            statement.executeUpdate();

            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            ConnectionFactory.close(connection);
            ConnectionFactory.close(statement);
        }


        return false;
    }

    private String createUpdateQuery(List <String> fields, String updateBy) {
        StringBuilder sb = new StringBuilder();
        StringJoiner sj = new StringJoiner(",");

        sb.append("UPDATE ");
        sb.append(type.getSimpleName());
        sb.append(" SET ");

        for(String s: fields){
            StringBuilder crt = new StringBuilder(s);
            crt.append("=?");

            sj.add(crt);
        }

        sb.append(sj);
        sb.append(" WHERE ");
        sb.append(updateBy);
        sb.append("=?");

        return sb.toString();
    }

    public boolean updateById(T t){
        List <String> fields = new ArrayList<>();
        List <Object> values = new ArrayList<>();
        String id = null;

        for (Field field: type.getDeclaredFields()) {

            field.setAccessible(true);
            if (field.getName() != "id") {

                fields.add(field.getName());
                try {
                    values.add(field.get(t));
                } catch (IllegalArgumentException | IllegalAccessException e) {
                    e.printStackTrace();
                }

            } else {
                try {
                    id = field.get(t).toString();
                } catch (IllegalArgumentException | IllegalAccessException e) {
                    e.printStackTrace();
                }
            }

        }

        Connection connection = null;
        PreparedStatement statement = null;

        try {
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(createUpdateQuery(fields, "id"));

            int i = 1;
            for (Object obj: values) {
                statement.setString(values.indexOf(obj) + 1, obj.toString()); //set the SET fields
                i ++;
            }
            //set the ID field
            statement.setString(i, id);

            statement.executeUpdate();

            return true;

        }catch (SQLException e){
            e.printStackTrace();
        } finally {
            ConnectionFactory.close(connection);
            ConnectionFactory.close(statement);
        }




        return false;
    }

    public List<T> findAll(){
        Connection connection = null;
        ResultSet result = null;
        PreparedStatement statement = null;


        try {
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement("SELECT * FROM " + type.getSimpleName());
            result = statement.executeQuery();

            return createObject(result);

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (NoResultException e) {
            e.printStackTrace();
        } finally {
            ConnectionFactory.close(connection);
            ConnectionFactory.close(result);
            ConnectionFactory.close(statement);
        }

        return null;
    }

}
